#include "TcpClient.h"

using namespace boost::asio;
using ip::tcp;

TcpClient::TcpClient(const std::string& hostName, const std::string& port)
:   socket(iOService),
    hostName(hostName),
    port(port)
{
    try {
        connectOrThrowException();
    } catch (std::runtime_error& e) {
        std::cerr << e.what() << std::endl;
    }
}

void TcpClient::connectOrThrowException()
{
    resolveHost();
    connect(socket, endpointIterator, error);
    checkError("Connection failed: ");
}

void TcpClient::resolveHost()
{
    tcp::resolver resolver(iOService);
    tcp::resolver::query query(hostName, port);
    endpointIterator = resolver.resolve(query);
}

bool TcpClient::checkError(const std::string &info) const
{
    if(error && error != error::eof) {
        std::string errorMessage = info + error.message();
        throw std::runtime_error(errorMessage);
    }
    return false;
}

void TcpClient::write(const std::string& msg)
{
    boost::asio::write(socket, buffer(msg), error );
    checkError("Send failed: ");
}

std::string TcpClient::read(const std::string& delim)
{
    boost::asio::streambuf receiveBuffer;
    boost::asio::read_until(socket, receiveBuffer, delim,error);

    if(!checkError("Receive failed: "))
    {
        const char* response = buffer_cast<const char*>(receiveBuffer.data());
        std::string data = response;
        data += "\n";

        return data;
    }

    return "";
}