#ifndef PROJEKT_TCPCLIENT_H
#define PROJEKT_TCPCLIENT_H

#include <iostream>
#include <boost/asio.hpp>

class TcpClient
{
public:
    TcpClient(const std::string& hostName, const std::string& port);

protected:
    void write(const std::string& msg);
    std::string read(const std::string& delim = "\r\n");

private:
    boost::asio::io_service iOService;
    boost::asio::ip::tcp::socket socket;
    std::string hostName;
    std::string port;
    boost::asio::ip::tcp::resolver::iterator endpointIterator;
    boost::system::error_code error;

    void connectOrThrowException();
    void resolveHost();
    bool checkError(const std::string& info) const;
};

#endif //PROJEKT_TCPCLIENT_H