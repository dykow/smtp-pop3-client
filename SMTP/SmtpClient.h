#ifndef PROJEKT_SMTPCLIENT_H
#define PROJEKT_SMTPCLIENT_H

#include <iostream>
#include "../TCP/TcpClient.h"
#include "../helpers.h"

class SmtpClient: TcpClient
{
public:
    SmtpClient(const std::string&  host, const std::string& port)
    :   TcpClient(host, port),
        host(host) {}

    void setServerCredentials(const std::string& username, const std::string& password)
    {
        this->username = encodeBase64(username);
        this->password = encodeBase64(password);
    }

    void prepareEmail (const std::string& recipient,
            const std::string& sender,
            const std::string& subject,
            const std::string& message)
    {
        this->recipient = recipient;
        this->sender = sender;
        this->subject = subject;
        this->message = message;
    }

    void sendEmail();

private:
    std::string host;
    std::string username;
    std::string password;
    std::string sender;
    std::string recipient;
    std::string subject;
    std::string message;

    void sayEhlo();
    void authenticate();
    void bundleSenderAndRecipient();
    void bundleMailBody();
    void sayBye();
};

#endif //PROJEKT_SMTPCLIENT_H