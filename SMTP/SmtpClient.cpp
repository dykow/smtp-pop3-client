#include "SmtpClient.h"

#define EHLO "EHLO "
#define AUTH_LOGIN "AUTH LOGIN\r\n"
#define MAIL_FROM "MAIL FROM: "
#define RCPT_TO "RCPT TO: "
#define DATA "DATA\r\n"
#define QUIT "quit\r\n"

void SmtpClient::sendEmail()
{
    try {
        sayEhlo();
        authenticate();
        bundleSenderAndRecipient();
        bundleMailBody();
        sayBye();
    } catch (std::runtime_error& e) {
        std::cerr << e.what() << std::endl;
    }
}

// This is an example
// EHLO smtp.mailtrap.io
// 250-mailtrap.io
// 250-SIZE 5242880
// 250-PIPELINING
// 250-ENHANCEDSTATUSCODES
// 250-8BITMIME
// 250-DSN
// 250-AUTH PLAIN LOGIN CRAM-MD5
// 250 STARTTLS
void SmtpClient::sayEhlo()
{
    read();
    write(EHLO + host + "\r\n");
    read();
}

// AUTH LOGIN
// 334 ...
// username encoded in base64
// 334 ...
// password encoded in base64
// 235 2.0.0 OK
void SmtpClient::authenticate()
{
    write(AUTH_LOGIN);
    read();
    write(username + "\r\n");
    read();
    write(password + "\r\n");
    read();
}

// MAIL FROM: <from@smtp.mailtrap.io>
// 250 2.1.0 Ok
// RCPT TO: <to@smtp.mailtrap.io>
// 250 2.1.0 Ok
void SmtpClient::bundleSenderAndRecipient()
{
    const std::string mailFrom = "<" + sender + ">\r\n";
    write(MAIL_FROM + mailFrom);
    read();

    const std::string mailTo = "<" + recipient + ">\r\n";
    write(RCPT_TO + mailTo);
    read();
}

// DATA
// 354 Go ahead
// To: to@smtp.mailtrap.io
// From: from@smtp.mailtrap.io
// Subject: Hello world!
//
// Message
// .
// 250 2.0.0 Ok: queued
void SmtpClient::bundleMailBody()
{
    write(DATA);
    read();
    write("To: " + recipient + "\r\n");
    write("From: " + sender + "\r\n");
    write("Subject: " + subject + "\r\n");
    write("\r\n");
    write(message + "\r\n");
    write(".\r\n");

    const std::string response = read();
    if (response != "250 2.0.0 Ok: queued\r\n\n") {
        std::cerr << "Error! Message has not been sent.\n" << response;
        exit(1);
    } else
        std::cout << "\nMessage sent!\n";
}

// quit
// 221 2.0.0 Bye
// Connection closed by foreign host.
void SmtpClient::sayBye()
{
    write(QUIT);
    read();
}