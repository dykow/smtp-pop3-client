_This is the copy of the private repo. No commits history_

# SMTP/POP3 client

You need Boost.Asio to build it.

**Usage instructions:**
Run program with the following parameters:

To send: `mailer send -h {hostname} -p {port} -l {login} -pas {password} -f {from@mail.com} -t {to@another.com} -s {subject} -m {msg}`  
To get: `mailer get -h {hostname} -p {port} -l {login} -pas {password}`


Useless doc can be found [here](https://mailer-doc.netlify.com/). Doc comments were deleted from the actual code.