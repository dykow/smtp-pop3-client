#ifndef PROJEKT_POP3CLIENT_H
#define PROJEKT_POP3CLIENT_H

#include <iostream>
#include "../TCP/TcpClient.h"
#include "../helpers.h"

class Pop3Client: public TcpClient
{
public:
    Pop3Client(const std::string&  host, const std::string& port)
    :   TcpClient(host, port) {}

    void setServerCredentials(const std::string& username, const std::string& password)
    {
        this->username = encodeBase64(username);
        this->password = encodeBase64(password);
    }

    void showAll();


private:
    std::string username;
    std::string password;
    std::vector<std::string> messages;

    void getAll(const std::vector<std::string>& msgIndexesAndSizes);
    std::vector<std::string> list();
    static std::vector<std::string> parseList(const std::string& str, const std::string& delim);
    void authenticate();
    void sayBye();
};


#endif //PROJEKT_POP3CLIENT_H
