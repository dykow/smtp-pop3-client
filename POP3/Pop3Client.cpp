#include "Pop3Client.h"

#define AUTH_LOGIN "AUTH LOGIN\r\n"
#define RETR "RETR "
#define LIST "LIST\r\n"
#define QUIT "QUIT\r\n"

using namespace std;

void Pop3Client::showAll()
{
    try {
        authenticate();
        getAll(list());
        sayBye();
    } catch (runtime_error& e) {
        cerr << e.what() << endl;
    }

    if(!messages.empty())
        for(const auto& i : messages)
            cout << i << endl;
    else
        cout << "No messages." << endl;
}

void Pop3Client::authenticate()
{
    read();
    write(AUTH_LOGIN);
    read();
    write(username + "\r\n");
    read();
    write(password + "\r\n");
    read();
}

vector<string> Pop3Client::list()
{
    write(LIST);
    read();
    string response = read(".\r\n");
    return parseList(response, "\r\n");
}

vector<string> Pop3Client::parseList(const string& s, const string& delim)
{
    vector<string> vec;
    vec.reserve(30);
    size_t last = 0;
    size_t next = 0;
    while((next = s.find(delim, last)) != string::npos)
    {
        string current = s.substr(last, next - last);
        vec.push_back(current);
        last = next + 2;
    }
    vec.erase(vec.end());
    return vec;
}

void Pop3Client::getAll(const vector<string>& msgIndexesAndSizes)
{
    messages.resize(msgIndexesAndSizes.size());
    unsigned int messagesIndexes[msgIndexesAndSizes.size()];
    unsigned int j = 0;
    for(const auto& i : msgIndexesAndSizes)
    {
        istringstream iss(i);
        iss >> messagesIndexes[j];
        string index = to_string(messagesIndexes[j]);
//      sleep is only for mailtrap.io because it allows to make only certain amount of requests
        if(j % 19 == 0)
            sleep(8);
        write(RETR + index + "\r\n");
        messages[j] = read(".\r\n");
        ++j;
    }
}

void Pop3Client::sayBye()
{
    write(QUIT);
    read();
}