#include "SMTP/SmtpClient.h"
#include "POP3/Pop3Client.h"

using namespace std;

void showHelp()
{
    cout << "\nRun program with the following parameters\n"
        << "To send: send -h hostname -p port -l login -pas password -f from -t to -s subject -m msg\n"
        << "To get: get -h hostname -p port -l login -pas password\n";
}

bool isNotAFlag(const string& s)
{
    return s != "-h" && s != "-p" && s != "-l" && s != "-pas" && s != "-f" && s != "-t" && s != "-s" && s != "-m";
}

int main(int argc, char** argv)
{
    if(argc > 1) {
        if(strcmp(argv[1], "send") == 0)
        {
            string host, port, login, pas, from, to, subject, msg;
            for(int i = 2; i < argc; i++) {
                if(strcmp(argv[i], "-h") == 0) {
                    host = argv[i + 1];
                    ++i;
                }
                else if(strcmp(argv[i], "-p") == 0){
                    port = argv[i + 1];
                    ++i;
                }
                else if(strcmp(argv[i], "-l") == 0) {
                    login = argv[i + 1];
                    ++i;
                }
                else if(strcmp(argv[i], "-pas") == 0) {
                    pas = argv[i + 1];
                    ++i;
                }
                else if(strcmp(argv[i], "-f") == 0) {
                    from = argv[i + 1];
                    ++i;
                }
                else if(strcmp(argv[i], "-t") == 0) {
                    to = argv[i + 1];
                    ++i;
                }
                else if(strcmp(argv[i], "-s") == 0) {
                    ++i;
                    while(isNotAFlag(argv[i])) {
                        subject += argv[i];
                        subject += " ";
                        if(i < argc - 1)
                            ++i;
                        else
                            break;
                    }
                    subject.erase(subject.length() - 1, 1);
                    --i;
                }
                else if(strcmp(argv[i], "-m") == 0) {
                    ++i;
                    while(isNotAFlag(argv[i])) {
                        msg += argv[i];
                        msg += " ";
                        if(i < argc - 1)
                            ++i;
                        else
                            break;
                    }
                    msg.erase(msg.length() - 1, 1);
                    --i;
                }
            }
            SmtpClient smtpClient(host, port);
            smtpClient.setServerCredentials(login, pas);
            smtpClient.prepareEmail(to, from, subject, msg);
            smtpClient.sendEmail();
        } else if(strcmp(argv[1], "get") == 0) {
            string host, port, login, pas;
            for(int i = 2; i < argc; i += 2) {
                if(strcmp(argv[i], "-h") == 0)
                    host = argv[i + 1];
                else if(strcmp(argv[i], "-p") == 0)
                    port = argv[i + 1];
                else if(strcmp(argv[i], "-l") == 0)
                    login = argv[i + 1];
                else if(strcmp(argv[i], "-pas") == 0)
                    pas = argv[i + 1];
            }
            Pop3Client pop3Client(host, port) ;
            pop3Client.setServerCredentials(login, pas);
            pop3Client.showAll();
        } else {
            showHelp();
            exit(2);
        }
    } else {
        showHelp();
        exit(2);
    }

    return 0;
}