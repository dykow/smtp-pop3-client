#ifndef PROJEKT_HELPERS_H
#define PROJEKT_HELPERS_H

#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/algorithm/string.hpp>

std::string encodeBase64(const std::string &val);

#endif //PROJEKT_HELPERS_H
